//
//  CurrencyServiceTests.swift
//  SampleProjectTests
//
//  Created by Vamsee on 28/01/2020.
//  Copyright © 2020 ProKarma. All rights reserved.
//

import XCTest
@testable import SampleProject

class CurrencyServiceTests: XCTestCase {
    
    func testCancelRequest() {
        
        // giving a "previous" session
        CurrencyService.shared.fetchConverter { (_) in
            // ignore call
        }
        
        // Expected to task nil after cancel
        CurrencyService.shared.cancelFetchCurrencies()
        XCTAssertNil(CurrencyService.shared.task, "Expected task nil")
    }
}
