//
//  Currency.swift
//  SampleProject
//
//  Created by Vamsee on 13/12/2019.
//  Copyright © 2019 ProKarma. All rights reserved.
//

import Foundation

enum Currency : String {
    case EUR
    case GBP
    case USD
}

enum CurrencyLocale : String {
    case EUR = "fr_FR"
    case GBP = "en_UK"
}

struct CurrencyRate {
    
    let currencyIso : String
    let rate : Double
}
