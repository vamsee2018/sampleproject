//
//  Converter.swift
//  SampleProject
//
//  Created by Vamsee on 13/12/2019.
//  Copyright © 2019 ProKarma. All rights reserved.
//

import Foundation

struct CurrencyConverter {
    
    let base : String
    let date : String
    
    let rates : [CurrencyRate]
}

extension CurrencyConverter : Parceable {
    
    static func parseObject(dictionary: [String : AnyObject]) -> Result<CurrencyConverter, ErrorResult> {
        if let base = dictionary["base"] as? String,
            let date = dictionary["date"] as? String,
            let rates = dictionary["rates"] as? [String: Double] {
            
            let finalRates : [CurrencyRate] = rates.compactMap({ CurrencyRate(currencyIso: $0.key, rate: $0.value) })
            let conversion = CurrencyConverter(base: base, date: date, rates: finalRates)
            
            return Result.success(conversion)
        } else {
            return Result.failure(ErrorResult.parser(string: "Unable to parse conversion rate"))
        }
    }
}
