//
//  RequestFactory.swift
//  SampleProject
//
//  Created by Vamsee on 14/12/2019.
//  Copyright © 2019 ProKarma. All rights reserved.
//

import Foundation

final class RequestFactory {
    
    enum Method: String {
        case GET
        case POST
        case PUT
        case DELETE
        case PATCH
    }
    
    static func request(method: Method, url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        return request
    }
}
