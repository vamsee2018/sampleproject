//
//  Result.swift
//  SampleProject
//
//  Created by Vamsee on 15/12/2019.
//  Copyright © 2019 ProKarma. All rights reserved.
//

import Foundation

enum Result<T, E: Error> {
    case success(T)
    case failure(E)
}
