//
//  File.swift
//  SampleProject
//
//  Created by Vamsee on 15/12/2019.
//  Copyright © 2019 ProKarma. All rights reserved.
//

import Foundation

enum ErrorResult: Error {
    case network(string: String)
    case parser(string: String)
    case custom(string: String)
}
