//
//  CurrencyService.swift
//  SampleProject
//
//  Created by Vamsee on 15/12/2019.
//  Copyright © 2019 ProKarma. All rights reserved.
//

import Foundation

protocol CurrencyServiceProtocol : class {
    func fetchConverter(_ completion: @escaping ((Result<CurrencyConverter, ErrorResult>) -> Void))
}

final class CurrencyService : RequestHandler, CurrencyServiceProtocol {
    
    static let shared = CurrencyService()
    
    let endpoint = "https://api.fixer.io/latest?base=GBP"
    var task : URLSessionTask?
    
    func fetchConverter(_ completion: @escaping ((Result<CurrencyConverter, ErrorResult>) -> Void)) {
        
        // cancel previous request if already in progress
        self.cancelFetchCurrencies()
        
        task = RequestService().loadData(urlString: endpoint, completion: self.networkResult(completion: completion))
    }
    
    func cancelFetchCurrencies() {
        
        if let task = task {
            task.cancel()
        }
        task = nil
    }
}
